Session and lab notes for The Ketones Group.

# Playlists on Spotify

These correspond to the chord progression notes in `progressions/`:

* [IV V iii vi](https://open.spotify.com/user/lionbark/playlist/3HV23gJphFwyZ3jN8Xbv2j)
* [IV I V vi](https://open.spotify.com/user/lionbark/playlist/7Bq16m6vVt2xBnQN1hdfGb)
* [IV I vi V](https://open.spotify.com/user/lionbark/playlist/3QbumhH6MAez1WjnxolyFq)
* [IV I vi iii](https://open.spotify.com/user/lionbark/playlist/3IEqWDm5CXBKPSeZokmgGi)

* [I V vi IV](https://open.spotify.com/user/lionbark/playlist/0THLwCQIv60IbRQsmYOXji)

* [vi V IV I](https://open.spotify.com/user/lionbark/playlist/0P4sdgWmJfrraIfJu2NC4j)
* [vi IV V I](https://open.spotify.com/user/lionbark/playlist/6u3fTEnLDwtQvYkIjQJuy8)
* [vi IV V iii](https://open.spotify.com/user/lionbark/playlist/31cA1bZq1oilA4ims8yf4s)
* [vi IV I IV](https://open.spotify.com/user/lionbark/playlist/15HyaYOu7n8tmCdzRPIsdv)
* [vi I V IV](https://open.spotify.com/user/lionbark/playlist/3SAvWbeYFN2IERPc0vW2lf)
* [vi V IV V](https://play.spotify.com/user/lionbark/playlist/45ecqZmjERgbmBwEMYvZKJ)
