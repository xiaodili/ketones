It'll be like a game:

You record yourself doing one song which then segway's into another.

[Verse]     
G        C            D        G                  
How many roads must a man walk down, before you 
C          G    G        C           D           
call him a man, How many seas must a white dove
G               C             D             G
sail before she sleeps in the sand, Yes and how 
     C              D           G              
many times must the cannonballs fly before they're
C       G
forever banned,

[Chorus]
C            D         G                C   
The answer my friend  is blowin' in the wind
              D              G
The answer is blowin' in the wind

G              D            Am
Mama take this badge off of me
G       D          C
I can't use it any-more
G            D                        Am
It's getting dark, too dark to see
G          D                   C   
I feel I'm knockin on heaven's door
 
 
[Chorus]

G             D                    Am
Knock, knock, knockin' on heaven's door
G             D                    C   
Knock, knock, knockin' on heaven's door
G             D                    Am
Knock, knock, knockin' on heaven's door
G             D                    C   
Knock, knock, knockin' on heaven's door

Next verse + chorus:

G           D           Am
Mama put my guns in the ground
G       D              C
I can't shoot them any-more
G               D               Am
That long black cloud is comin' down
G          D                    C   
I feel I'm knockin' on heaven's door
 

[Chorus]

G             D                    Am
Knock, knock, knockin' on heaven's door
G             D                    C   
Knock, knock, knockin' on heaven's door
G             D                    Am
Knock, knock, knockin' on heaven's door
G             D                    C   
Knock, knock, knockin' on heaven's door
