# Notes

### Major Chords

```
1 2  3   4  5 6  7
I ii iii IV V vi viidim
```

### Minor Chords

#### Natural

```
1 2     3   4  5 6  7
i iidim III iv v VI VII
```

Shifting enharmonically:

* Major → Minor: 3 semitones down (e.g. C major to A minor)
* Minor → Major: 3 semitones up (e.g. A minor to C major)

#### Harmonic

Sounds Eygptian; raised 7th

```
1 2     3      4  5 6  7
i iidim IIIaug iv V VI viidim
```

#### Melodic

* Raised 6th, 7th when ascending

```
1 2     3   4  5 6     7
i ii IIIaug IV V vidim viidim
```

* Natural when descending

```
1 2     3   4  5 6  7
i iidim III iv v VI VII
```

# Guitar

## Barr positions

e  -----------
   | | | | | |
B  -----------
   | | | | | |
G  -----------
   | | | | | |
D  -----------
   | | | | | |
A  -----------
   | | | | | |
E  -----------

### 1st string voicings

Root position major         1st inversion major             2nd inversion major

e  -o---------              e  -o---------                  e  -o---------
   | | | | | |                 | | | | | |                     | | | | | |
B  -----o-----              B  -o---------                  B  ---o-------
   | | | | | |                 | | | | | |                     | | | | | |
G  -----o-----              G  ---o-------                  G  -o---------
   | | | | | |                 | | | | | |                     | | | | | |
D  -----o-----              D  -----o-----                  D  -----o-----
   | | | | | |                 | | | | | |                     | | | | | |
A  -o---------              A  -----o-----                  A  -------o---
   | | | | | |                 | | | | | |                     | | | | | |
E  -o---------              E  -o---------                  E  ----------- x

Root position minor         1st inversion minor             2nd inversion minor

e  -o---------              e  -o---------                  e  -o---------
   | | | | | |                 | | | | | |                     | | | | | |
B  ---o-------              B  -o---------                  B  -----o-----
   | | | | | |                 | | | | | |                     | | | | | |
G  -----o-----              G  -o---------                  G  ---o-------
   | | | | | |                 | | | | | |                     | | | | | |
D  -----o-----              D  -----o-----                  D  -----o-----
   | | | | | |                 | | | | | |                     | | | | | |
A  -o---------              A  -----o-----                  A  ----------- x
   | | | | | |                 | | | | | |                     | | | | | |
E  -o---------              E  -o---------                  E  ----------- x

# Glossary

* [Secondary Dominant](https://music.stackexchange.com/questions/22057/what-is-a-secondary-dominant-chord)
* [Diminished Seventh](https://en.wikipedia.org/wiki/Diminished_seventh_chord)
