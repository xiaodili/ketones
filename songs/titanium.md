# Titanium/Umbrella/Viva La Vida

You shout it out,
But I can't hear a word you say
I'm talking loud, not saying much
I'm criticized but all your bullets ricochet
You shoot me down, but I get up

I'm bulletproof, nothing to lose
Fire away, fire away
Ricochet, you take your aim
Fire away, fire away
You shoot me down but I won't fall
I am titanium
You shoot me down but I won't fall
I am titanium

(Umbrella verse)

These fancy things, will never come in between
You're part of my entity, here for infinity
When the war has took its part
When the world has dealt its cards
If the hand is hard, together we'll mend your heart
Because

When the sun shines, we'll shine together
Told you I'd be here forever
Said I'll always be a friend
Took an oath I'ma stick it out 'til the end
Now that it's raining more than ever
Know that we'll still have each other
You can stand under my umbrella
You can stand under my umbrella
(Ella ella eh eh eh)
Under my umbrella
(Ella ella eh eh eh)
Under my umbrella

Stone-heart, machine gun
Firing at the ones who run
Stone heart loves bulletproof glass

I hear Jerusalem bells a-ringing
Roman cavalry choirs are singing
Be my mirror, my sword and shield
My missionaries in a foreign field
For some reason I can't explain
I know St Peter won't call my name
Never an honest word
But that was when I ruled the world
