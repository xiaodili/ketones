"9 Crimes"
(feat. Lisa Hannigan)

Amin F C G (C major)

vi VI I V

```

Leave me out with the waste
This is not what I do
It's the wrong kind of place
To be thinking of you
It's the wrong time
For somebody new
It's a small crime
And I've got no excuse

Is that alright?
Give my gun away when it's loaded
Is that alright?
If you don't shoot it how am I supposed to hold it
Is that alright?

(cajon on off-beat?)
Give my gun away when it's loaded

Is that alright
With you?

(Damien)

         (Amin)       (F)
Leave me out with the waste
        (C)        (G)
This is not what I do
         (A)           (F)
It's the wrong kind of place
      (C)         (G)
To be cheating on you
         (Amin)(F)
It's the wrong time
      (C)        (G)
She's pulling me through
       (Amin) (F)
It's a small crime
         (C)    (G)
And I've got no excuse

Is that alright?
              (Is that alright?)
I give my gun away when it's loaded
Is that alright?
                                (Is that alright?)
If you don't shoot it, how am I supposed to hold it
Is that alright?
                   (Is that alright?)
I give my gun away when it's loaded

Is that alright
Is that alright with you?

Is that alright?
I give my gun away when it's loaded
Is that alright?
If you don't shoot it, how am I supposed to hold it
Is that alright?
If I give my gun away when it's loaded
Is that alright
Is that alright with you?

Is that alright?
Is that alright?
Is that alright with you?

Is that alright?
Is that alright?
Is that alright with you?

No...
```
