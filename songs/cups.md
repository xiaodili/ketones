# Cups

Verse 1
C
I got my ticket for the long way 'round
F                             C
Two bottle 'a whiskey for the way
      Am         G         F
And I sure would like some sweet company
        Fmaj7   G                   C
And I'm leaving tomorrow, wha-do-ya say?


Chorus:
         Am
When I'm gone
         F
When I'm gone
C                             G
You're gonna miss me when I'm gone
             Am            G
You're gonna miss me by my hair
             F
You're gonna miss me everywhere, oh
Fmaj7        G                C
You're gonna miss me when I'm gone

(Repeat Chorus)


Verse 2
C
I've got my ticket for the long way 'round
F                             C 
The one with the prettiest of views
         Am                  G                F
It's got mountains, it's got rivers, it's got sights to give you shivers
       Fmaj7         G             C
But it sure would be prettier with you
 

Chorus:
         Am
When I'm gone
         F
When I'm gone
C                             G
You're gonna miss me when I'm gone
             Am            G
You're gonna miss me by my hair
             F            Fm
You're gonna miss me everywhere, oh
Fmaj7        G                C
You're gonna miss me when I'm gone

(Repeat Chorus)
