# Aimee Mann Medley

(Aimee Manndley)

## Stupid Thing

Key: F major

I: F major
ii: G minor
iii: A minor
IV: Bb major
V: C major
v: C minor
vi: D minor
VIIb: Eb major


```
I           iii        vi       IV
Nothing was saving our day
          I          iii 
There was nothing to say
        VIIb           IV
        Eb
But you said something anyway
I          iii            vi    IV
Claiming I stepped out of line
      I             iii
Which forced you to leave me
   VIIb    IV       iii        
As if that idea was mine


ii             IV
Oh, you stupid thing
I                          V
Speaking of course as your dear departed
ii             IV
Oh, you stupid thing
I                 V
It wasn't me that you outsmarted
ii             IV
Oh, you stupid thing
            I
Stopping it all
          VIIb IV    I
Before it even start-ed


I         iii           vi     IV 
I bet you knew it would come
I                iii        VIIb
That's just like you to sit back
         IV
And just play it dumb
I           iii           vi          IV
One word of warning would help
    I         iii
But that sacrifice was made
VIIb      IV    IV/iii
Trying to save yourself

ii             IV
Oh, you stupid thing
I                          V
Speaking of course as your dear departed
ii             IV
Oh, you stupid thing
I                 V
It wasn't me that you outsmarted
ii             IV
Oh, you stupid thing
            I
Stopping it all
          VIIb IV    I
Before it even start-ed

Solo:
vi IV I v IV I I
vi vi IV I v IV VIIv IV I


I            iii        vi   IV
Maybe that's just how I am
   I              iii
To fall where I stand
       VIIb                  IV
Or I'm weak for that kind of man
One who looks helpless and brave
        I       
But you turned into a coward
I don't care for the parts you saved

ii             IV
Oh, you stupid thing
I                          V
Speaking of course as your dear departed
ii             IV
Oh, you stupid thing
I                 V
It wasn't me that you outsmarted

Oh, you stupid thing...
```

## Voice Carry

```
I'm in the dark, I'd like to read his mind
But I'm frightened of the things I might find
Oh, there must be something he's thinking of
To tear him away-a-ay
When I tell him that I'm falling in love
Why does he say-a-ay

Hush hush, keep it down now, voices carry
Hush hush, keep it down now, voices carry
Uh-ah

I try so hard not to get upset
Because I know all the trouble I'll get
Oh, he tells me tears are something to hide
And something to fear-eh-eh
And I try so hard to keep it inside
So no one can hear

Hush hush, keep it down now, voices carry
Hush hush, keep it down now, voices carry
Hush hush, keep it down now, voices carry
Uh-ah

Oh!
He wants me, but only part of the time
He wants me, if he can keep me in line

Hush hush, keep it down now, voices carry
Hush hush, keep it down now, voices carry
Hush hush, shut up now, voices carry
Hush hush, keep it down now, voices carry
Hush hush, darling, she might overhear
Hush, hush - voices carry
He said shut up - he said shut up
Oh God can't you keep it down
Voices carry
Hush hush, voices carry
```

## Freeway

Key: Eb major

I: Eb major
ii: F minor
iii: G minor
IV: Ab major
V: Bb major
vi: C minor

Intro:

IV I

Verse:

IV I V

IV I 

Chorus:

```
       IV           I             V                         IV I IV I
You've got a lot of money but you can't afford the freeway

    IV             I                 V                      IV I IV I
The road to Orange Country leaves an awful lot of leeway
      IV           I           V                            IV I IV I
Where everyone's a doctor or a specialist in retail
    IV                   I         V                                    IV I IV I
They'll sell you all the speed you want if you can take the blackmail

vi
You know it
I
I know it
V
Why don't you
ii
Just show it
    IV                     I         V                            IV I IV I
You got a lot of money but you can't afford the freeway
    IV                     I         V                            IV I IV I
You got a lot of money but you can't afford the freeway

       IV           I         V                                   IV I IV I
You've got a lot of money but you cannot keep your bills paid
    IV           I                V                               IV I IV I
The sacrifice is worth it just to hang around the arcade
    vi               V              ii                            IV I IV I
You found yourself a prophet but you left him on the boardwalk
    vi                   V              ii                        IV I IV I
Another chocolate Easter bunny, hollowed out by your talk

vi
You know it
I
I know it
V
Why don't you
ii
Just show it
You got a lot of money but you can't afford the freeway
You got a lot of money but you can't afford the freeway

And everything I do is wrong
                        V
But at least I'm hanging on
    IV           I              V       
You got a lot of money that you can't afford
    IV           I              V       
You got a lot of money that you can't afford
    IV           I              V               ii
You got a lot of money but you can't afford the freeway
    IV
You got a lot of money but you can't afford the freeway
You got a lot of money but you can't afford
    vi           V         ii
You got a lot of money but you can't afford
```

## Red Vines

Key: Eb major

Eb	F	G	Ab	Bb	C	D   Eb

I: Eb
ii: F minor
iii: G minor
IV: Ab major
V: Bb major
vi: C minor

Alternately, D# major (don't use; double sharp)

D#	E#	F## G#  A#  B#  C#  D#

2nd and 4th bars bass comes in on the offbeat

```
Intro:

F Bb Eb

IV/ii vi/iii /V

        I         vi             iii        IV   
They're all still on their honey-moon
     I          vi         iii      iii
Just read the dialogue bal-loon
         IV         I                 V
Everyone loves you, why should they not?
    I       vi          iii      IV
And I'm the only one who knows
     I     vi            iii 
That Disneyland's about to  close
           IV    I              V
I don't suppose you'd give it a shot
       II               ii
Knowing all that you've got

                   I          V      ii
Are cigarettes and Red Vines
                              I
Just close your eyes, 'cause, baby
          V             ii
You never do know
                   I             V
And I'll be on the sidelines,
        ii
With my hands tied,
             I      V   ii
Watching the show

          I          vi         iii      IV
Well, it's always fun and games until
          I          vi         iii      iii
It's clear you haven't got the skill
           IV    I                V
In keeping the gag from going too far
          I         vi          iii      IV
So you're running 'round the parking lot
     I     vi               iii    
'Til every lightning bug is caught
         IV         I                 V
Punching some pinholes in the lid of a jar
      II               ii
While we wait in the car

                   I          V      ii
Are cigarettes and Red Vines
                              I
Just close your eyes, 'cause, baby
          V             ii
You never do know
                   I             V
And I'll be on the sidelines,
        ii
With my hands tied,
             I      V   ii
Watching the show
                      IV   vi   I
And tell me, would it kill you
                IV   vi    I
Would it really spoil everything
              IV    vi    
If you didn't blame yourself
II               ii
do you know what I mean?

                   I          V      ii
Are cigarettes and Red Vines
                              I
Just close your eyes, 'cause, baby
          V             ii
You never do know
                   I             V
And I'll be on the sidelines,
        ii
With my hands tied,
             I      V   ii
Watching the show
             I      V   ii
Watching the show
```

## Save Me

## Pavlov's Bell

C major

```
IV    vi  I               ii
Oh Mario, sit here by the window
IV             vi        IV
Stay here 'til we reach Idaho
IV          vi
And when we go
I               ii   
Hold my hand on take-off
IV             vi      IV
Tell me what I already know
              VIIb IV    I
That we can't talk about it
       VIIb  IV         I
No, we can't talk about it

        vi     I     
Because nobody knows
IV           II
That's how I nearly fell
vi      I     
Trading clothes
IV          II
And ringing Pavlov's Bell
vi      I      IV
History shows
              II7       II
There's not a chance in hell, but

IV    vi  I               ii
Oh Mario. we're only to Ohio
IV                 vi        IV
It's kinda getting harder to breathe
IV             vi
I won't let it show
I             vi
I'm all about denial
IV
But can't denial let me believe?
              VIIb IV    I
That we could talk about it
       VIIb  IV         I
But we can't talk about it

        vi     I     
Because nobody knows
IV           II
That's how I nearly fell
vi      I     
Trading clothes
IV          II
And ringing Pavlov's Bell
vi      I      IV
History shows
           II7      II
but rarely shows it well
Well, well, well

IV    vi  I               ii
Oh Mario, why if this is nothing
IV                 vi        IV
I'm finding it so hard to dismiss
IV             vi
If you're what I need,
I             vi
Then only you can save me
IV
So come on baby. give me the fix
               VIIb IV    I
And let's just talk about it
     VIIb   IV         I
I've got to talk about it

        vi     I     
Because nobody knows
IV           II
That's how I nearly fell
vi      I     
Trading clothes
IV          II
And ringing Pavlov's Bell
vi      I      IV
History shows
            II7      II
Like it was show and tell
So tell me
           vi       I     
That's how I nearly fell
   IV               II
By ringing Pavlov's Bell
          vi       I     
So, baby, show and tell
Oh Mario, Mario
```

# Deathly

D Major

D E F# G A B C# D

I: D major
ii: E minor
II7: E major seventh
iii: F# minor
IV: G major
V: A major
vi: B minor
vii: C# diminished

```
I             IV         V-vi
Now that I've met you
            iii     IV
Would you object to 
      II7         I     V
Never seeing each other again
       I       IV          vi
'Cause I can't afford to
      iii        IV
Climb aboard you
         II7           I      V
No one's got that much ego to spend

Prechorus

IV I     IV   I    V
So don't work your stuff
IV I    IV   I    vi         IV
Be-cause I've got troubles enough
IV  I     IV   I   V
No, don't pick on me
IV   I   IV  I  vi             IV
When one act of kindness could be

I    V    IV
Deathly
I     V   IV
Deathly

IV I  V
Deathly

      

       I          IV        vi
'Cause I'm just a problem
            iii     IV
For you to solve and
      II7             I            V
Watch dissolve in the heat of your charm
    I         IV          vi
But what will you do when
            iii            IV
You run it through and
         II7     I           V
You can't get me back on the farm

IV I     IV   I    V
So don't work your stuff
IV I    IV    I   vi         IV
Be-cause I've got troubles enough
IV  I     IV   I   V
No, don't pick on me
IV   I   IV  I   vi             IV
When one act of kindness could be

I    V  
Deathly
IV I  V
Deathly
IV I  V
Definitely

I              IV       vi
You're on your honor
             iii      IV
'Cause I'm a goner
        II7     I    V
And you haven't even begun
   I       IV       vi
So do me a favor
            iii      IV
If I should waver
      II7
Be my savior
    I           V
And get out the gun

IV I     IV   I    V
So don't work your stuff
IV I    IV    I   vi         IV
Be-cause I've got troubles enough
IV  I     IV   I   V
No, don't pick on me
IV   I   IV  I   vi             IV
When one act of kindness could be
I    V  
Deathly
IV I  V
Deathly
IV I  V
Definitely
```

## Humpty Dumpty

NB: See Ipponoke for up to date tab for this


A minor

i: A minor
IIb: Ab major
iidim: Bdim
III: C major
iv: D minor
v: E minor
VI: F major
VII: G major

```

i            v               i        v
Say you were split, you were split in fragments
    i           v            i       v
And none of the pieces would talk to you
i            v          III     VII
Wouldn't you want to be who you had been
VI           III       VII
Well, baby I want that too

   VI     III      VII      iv
So better take the keys and drive forever
 VI     III       VII           iv
Staying won't put these futures back together
VI      III     VII       iv
All the perfect drugs and superheroes
VI       III   VII             iv           IIb
Wouldn't be enough to bring me up to zero

i            v                  i            v
Baby, you're great, you've been more than patient
i           v       i     v
Saying it's not a catastrophe
But I'm not the girl you once put your faith in
VI           III       VII
Just someone who looks like me

   VI     III      VII      iv
So better take the keys and drive forever
 VI     III       VII           iv
Staying won't put these futures back together
VI      III     VII       iv
All the perfect drugs and superheroes
VI       III   VII             iv           IIb
Wouldn't be enough to bring me up to zero

   v                 i     v      i     VI    III    v
So get out while you can
    v             i     
Get out while you can
   v              i
Baby I'm pouring quicksand
    VI                   III
And sinking is all I had planned
               v        i    v     i   VI   iv    IIb
So better just go


   VI     III      VII      iv
So better take the keys and drive forever
 VI     III       VII           iv
Staying won't put these futures back together
VI      III     VII       iv
All the perfect drugs and superheroes
VI       III   VII             iv
Wouldn't be enough to bring me up to zero


   VI     III      VII      iv
All the king's horses and all the king's men
   VI     III      VII      iv
Couldn't put baby together again
   VI     III      VII      iv
All the king's horses and all the king's men
VI       III      VII      iv
Couldn't put baby together again

bII IV III

```
