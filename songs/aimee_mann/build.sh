#!/bin/bash
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

pushd $SCRIPT_DIR

lilypond="/Applications/LilyPond.app/Contents/Resources/bin/lilypond"

$lilypond deathly.ly;

open deathly.pdf;

popd
