\version "2.18.2"  % necessary for upgrading to future LilyPond versions.

\book {
  \header {
    title = "Deathly"
    composer = "Aimee Mann"
    tagline = "" %removes lilypad footer
    arranger = "Eddie Li" %removes lilypad footer
  }
  \score {
    \new PianoStaff <<
      \new Staff {
        \clef "treble"
        \relative d'' {
          \time 4/4
          \key d \major
          % Verse 1
          a                       g16    f8.            e8          f8        r4
          % Now                   that   I've           met         you
          % r16    r    r    r    r16    r    r    r    r16    r    r    r    r16    r    r    r   
            r8.              a,16 b8.              d16  e8          f8        r4          
          %                  wouldyou              obj- ject        to
          % r16    r    r    r    r16    r    r    r    r16    r    r    r    r16    r    r    r   
            r4                    b,16   d8.            e8          d8        r8          b8
          %                       ne     ver            see         ing                   each
          % r16    r    r    r    r16    r    r    r    r16    r    r    r    r16    r    r    r   
            b16    a8.            r8.              f16  e4                    r4
          % o      -ther                           a    -gain
          % r16    r    r    r    r16    r    r    r    r16    r    r    r    r16    r    r    r   
          \time 2/4
            r4                    r8.              f16
          %                                        cause
          % r16    r    r    r    r16    r    r    r    
          \time 4/4
            a'4                   g8.              f16  e8          f8        r4
          % I                     can't            af   -ford       to
          % r16    r    r    r    r16    r    r    r    r16    r    r    r    r16    r    r    r   
            r4                    b,8.             d16  e8          f8        r4
          %                       climb            a    -board      you
          % r16    r    r    r    r16    r    r    r    r16    r    r    r    r16    r    r    r   
            r4                    b,16    d8.            e8.         d         b4     
          %                       no     -one's         got         that      much
          % r16    r    r    r    r16    r    r    r    r16    r    r    r    r16    r    r    r   
            b16    a8.                             f16  e4                    r4
          % e      -go                             to   spend
          % r16    r    r    r    r16    r    r    r    r16    r    r    r    r16    r    r    r   
          \time 2/4
            r4                    r8.              f16
          %                  so   don't
          % r16    r    r    r    r16    r    r    r    
          \time 4/4
            a
          % work                                        your
          % r16    r    r    r    r16    r    r    r    r16    r    r    r    r16    r    r    r   
          % stuff                                                        be   -cause          
          % r16    r    r    r    r16    r    r    r    r16    r    r    r    r16    r    r    r   
          % I've                                        got
          % r16    r    r    r    r16    r    r    r    r16    r    r    r    r16    r    r    r   
          % trou                  -bles       en        -nough           no   don't
          % r16    r    r    r    r16    r    r    r    r16    r    r    r    r16    r    r    r   
          % pick                                        on
          % r16    r    r    r    r16    r    r    r    r16    r    r    r    r16    r    r    r   
          % me                                                           when one
          % r16    r    r    r    r16    r    r    r    r16    r    r    r    r16    r    r    r   
          % act                                         of
          % r16    r    r    r    r16    r    r    r    r16    r    r    r    r16    r    r    r   
          % kind                  -ness       could     be
          % r16    r    r    r    r16    r    r    r    r16    r    r    r    r16    r    r    r   

          % dea                                         -thly
          % r16    r    r    r    r16    r    r    r    r16    r    r    r    r16    r    r    r   
          %                                                                   dea
          % r16    r    r    r    r16    r    r    r    r16    r    r    r    r16    r    r    r   
          %                                             -thly
          % r16    r    r    r    r16    r    r    r    r16    r    r    r    r16    r    r    r   
          %                                                                   de
          % r16    r    r    r    r16    r    r    r    r16    r    r    r    r16    r    r    r   
          %                       fin         -ite      -ly                                    
          % r16    r    r    r    r16    r    r    r    r16    r    r    r    r16    r    r    r   
          \time 2/4
          %                                        -cause
          % r16    r    r    r    r16    r    r    r    

          % Verse 2
          \time 4/4

        }
      }
      %\new Staff {
        %\clef "bass"
        %\relative d {
          %\time 4/4
          %\key d \major
        %}
      %}
    >>
  }
}
