# Father and Son

Key: G

Chords

I: G
ii: A minor
iii: B minor
IV: C major
V: D major
vi: E minor
viidim: F# diminished

```
         G              D              C               Am7
         I              V              IV              ii7
It's not time to make a change, just relax and take it easy
             G                 Em
             I                 vi
You're still young that's your fault
           Am               D
           ii               V
there's so much you have to know
       G            D            C             Am7
       I            V            IV            ii7
Find a girl, settle down, if you want, you can marry
        G        Em           Am    C  G D
        I        vi           ii    IV I V
Look at me, I am old, but I'm happy
      G                 Bm7        C                 Am7
      I                 iii7       IV                ii7
I was once like you are now, and I know that its not easy
      G                 Em              Am  C  G D
      I                 vi              vi  IV I V
to be calm, when you've found something going on
              G             Bm7               C                 Am7
              I             iii7              IV                ii7
But take your time, think a lot, why think of everything you've got
             G               Em               D          G
             I               vi               V          I
For you will still be here tomorrow, but your dreams may not

let ring-------|
e|-------------3-----5--|
B|0--0-0-1--------------|
G|0--0-0-0--5-----5-----|
D|0--0-0-2--------------| x2
A|----------------------|
E|----------------------|


[Son]

        G          Bm             C            Am7
        I          iii            IV           ii7
How can I try to explain?  when I do he turns away again
     G               Em             Am    C  G D
     I               vi             ii    IV I V
it's always been the same, same old story
         G              Bm         C          Am7
         I              iii        IV         ii7
From the moment I could talk I was ordered to listen
              G         Em          D       G
              I         vi          V       I
now there's a way and I know that I have to go away
  D      C       G 
  V      IV      I 
I know I have to go

let ring-------|
e|-------------3-----5--|
B|0--0-0-1--------------|
G|0--0-0-0--5-----5-----|
D|0--0-0-2--------------| x2
A|----------------------|
E|----------------------|

[Interlude]
G Bm C Am7
G Em Am C G D
G Em C Am7
G Em D G
D C G

[Father]

         G              D                C                Am7
         I              V                IV               ii7
It's not time to make a change, just sit down and take it slowly
             G                  Em                Am
             I                  vi                ii
You're still young, that's your fault, there's so much you have
        D             G            D            C               Am 
        V             I            V            IV              ii
to go through, find a girl, settle down, if you want you can marry
        G        Em           Am    C G D
Look at me, I am old, but I'm happy


[Son]

        G               Bm             C                Am7
        I               iii            IV               ii7
All the times that I've cried, keeping all the things I knew inside
     G              Em          Am   D
     I              vi          ii   V
It's hard, but it's harder to ignore it
             G             Bm           C                   Am7
             I             iii          IV                  ii7
If they were right, I'd agree, but it's them they know, not me
              G          Em          D       G
              I          vi          V       I
Now there's a way, and I know that I have to go away
  D      C       G
  V      IV      I 
I know I have to go
```
