# Accidentally In Love

G Major

```
I       ii      iii     IV      V       vi      vii_dim
G       Amin    Bmin    C       D       E       F#_dim           
```

```
Intro:

I   IV  I   V     
G   C   G   D

I   IV  vi  II  IV   
G   C   Em  A   C (hold)

1st Verse:

G (I)                  C (IV)
So she said what's the problem baby
G (I)                     C (IV)
Whats the problem I don't know,
                  Em
well maybe I'm in love (love)
               A
Think about it every time I think about it
C
can't stop thinking 'bout it

G                       C
How much longer will it take to cure this
G                       C                      Em
Just to cure it cause I can't ignore it if its love (love)
               A                             D
Makes me wanna turn around and face me but I don't know
              C
nothing 'bout love

Chorus:

G        Am
Come on, come on
C             D
Turn a little faster
G        Am
Come on, come on
    C                 D
The world will follow after
G        Am
Come on, come on
C                 D     Em A C
Cause everybody's after love

2nd Verse:

G               C
So I said I'm a snowball running,
G                     C                             Em
running down into the spring that's coming all this love
              A                      C
Melting under blue skies belting out sunlight
           G
shimmering love
          C                  G
Well baby I surrender to the strawberry ice cream
C                          Em
never ever end of all this love
       A
Well I didn't mean to do it
            C
but there's no escaping your love

Bridge:

Em            C
These line of lightning mean we're
G            Am
never alone, never alone,

no, no

Extended Chorus:

G        Am
Come on, come on
C             D
Move a little closer
G        Am
Come on, come on
  C                D
I want to hear you whisper
G        Am
Come on, come on
C           D         Em D
Settle down inside my love

G        Am
Come on, come on
C             D
Jump a little higher
G        Am
Come on, come on
       C             D
If you feel a little lighter
G        Am
Come on, come on
   C                D       Em A C
We were once upon a time in love

We're accidentally in love


Interlude (x4):

                   G       C
We're accidentally in love
                   Em      D
We're accidentally in love


Pre-Chorus:

Accidentally

G            C
I'm in love, I'm in love
                    Em
I'm in love, I'm in love
                    D
I'm in love, I'm in love

Accidentally

G                   C
I'm in love, I'm in love
                    Em
I'm in love, I'm in love
                    D
I'm in love, I'm in love

Accidentally

Chorus:

G        Am
Come on, come on
C             D
Spin a little tighter
G        Am
Come on, come on
        C                D
And the world's a little brighter
G        Am
Come on, come on
     C            D          Em D G
Just get yourself inside her love
```
