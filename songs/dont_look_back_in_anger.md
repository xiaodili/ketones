```
C               G               Am
Slip inside the eye of your mind
          E7             F     G
Don't you know you might find
                    C        Am  G 
A better place to play


[Verse 2]

C             G               Am
You said that you'd never been
            E7                        F    G
But all the things that you've seen
            C         Am  G  
Slowly fade away


[Prechorus]

F               Fm              C   C
So I start a revolution from my bed
F                Fm                       C   C
Cos you said the brains I have went to my head
F                Fm              C    C
Step outside the summertime's in bloom
G
Stand up beside the fireplace
E7 
Take that look from off your face
Am                 G                F             G
Cos you ain't ever going to burn my heart ouuuuuuuuuut


[Chorus]

C   G        Am        E7             F
So Sally can wait, she knows it's too late
         G          C    Am  G 
As we're walking on by


    C    G      Am   E7             
Her soul slides away,   but don't look
F               G                  C     Am  G
back in anger          I heard you say


[Instrumental]

C      G        Am    E7    F    G     C    Am G


[Verse 3]

C               G               Am
Take me to the place where you go
          E7             F     G
Where nobody knows
                    C   Am G
If it's night or day


[Verse 4]

C               G               Am
Please don't put your life in the hands
          E7             F     G
Of a Rock and Roll band
                    C     Am G
Who'll throw it all away


[Prechorus]

[Chorus]

[Solo]


F  Fm  C
F  Fm  C
F  Fm  C
G
E7
Am G   F  G


[Chorus]

C   G        Am        E7             F
So Sally can wait, she knows it's too late
         G          C   Am G
As we're walking on by
C        G      Am        E7         F
Her soul slides away, but don't look back in anger
  G         C       Am G
I heard you say
C   G        Am        E7             F
So Sally can wait, she knows it's too late
         G          C     Am G
As we're walking on by
C        G      Am        
Her soul slides away, 
    F 
But don't look back in anger
Fm
Dont look back in anger
            C        C    G     Am   E7    F    G  
I heard you say
             C
At least not today
```

Verse + Chorus
C G Am E7
F G  C Am-G

PreChorus
F Fm C C
F Fm C C
F Fm C C
G G E7 E7
Am G  F  F
G G G G

Chorus
C G Am E7
F G C Am-G

Final part:
F
Fm
C G Am E7 F G
C


Slip inside the eye of your mind
Don't you know you might find
A better place to play

You said that you'd never been
But all the things that you've seen
Slowly fade away

So I start a revolution from my bed
Cos you said the brains I have went to my head
Step outside the summertime's in bloom
Stand up beside the fireplace
Take that look from off your face
Cos you ain't ever going to burn my heart ouuuuuuuuuut

So Sally can wait, she knows it's too late
As we're walking on by
Her soul slides away,   but don't look
back in anger          I heard you say

Take me to the place where you go
Where nobody knows
If it's night or day

Please don't put your life in the hands
Of a Rock and Roll band
Who'll throw it all away
