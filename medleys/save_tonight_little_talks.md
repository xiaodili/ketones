## Save Tonight / Little Talks

Amin F C G

```

(Piano intro)

Hey! Hey! Hey!

(Uke, single strum - Verse 1)

I don't like walking around this old and empty house
So hold my hand, I'll walk with you, my dear
The stairs creak as you sleep, it's keeping me awake
It's the house telling you to close your eyes

(Piano, with cajon - Bridge)

And some days I can't even dress myself
It's killing me to see you this way

'Cause though the truth may vary

This ship will carry our bodies safe to shore

(Piano riff)

(uke, strumming into Verse 2)

There's an old voice in my head that's holding me back
Well tell her that I miss our little talks
Soon it will be over and buried with our past
We used to play outside when we were young
And full of life and full of love.

(piano, cajon - Bridge)

Some days I don't know if I am wrong or right
Your mind is playing tricks on you, my dear

'Cause though the truth may vary

This ship will carry our bodies safe to shore

(cajon, piano)

                         (fight the break of dawn)
Don't listen to a word I say
Hey!
                          (tomorrow I'll be gone)
The screams all sound the same
Hey!
                          (fight the break of dawn)
Though the truth may vary This

ship will carry our bodies safe to shore

(fake leadup, bit of silence, switch to guitar)

Save tonight and fight the break of dawn
Come tomorrow - tomorrow I'll be gone
Save tonight and fight the break of dawn
Come tomorrow - tomorrow I'll be gone

          (Amin)
Go on and close the curtains
'Cause all we need is candlelight
You and me, and a bottle of wine
To hold you tonight (oh)

Save tonight and fight the break of dawn
Come tomorrow - tomorrow I'll be gone
Save tonight and fight the break of dawn
Come tomorrow - tomorrow I'll be gone

Well we know I'm going away
And how I wish - I wish it weren't so
So take this wine and drink with me
And let's delay our misery

                         (fight the break of dawn)
Don't listen to a word I say
Hey!
                          (tomorrow I'll be gone)
The screams all sound the same
Hey!
                          (fight the break of dawn)
Though the truth may vary This
                                   (tomorrow I'll be gone)
ship will carry our bodies safe to shore

(Piano/Uke strumming - soft outro

Though the truth may vary
This ship will carry our bodies safe to shore

Though the truth may vary
This ship will carry our bodies safe to shore

Though the truth may vary
This ship will carry our bodies safe to shore
```

## Lyrics

### Little Talks

(C# major)

```
Hey! Hey! Hey!
I don't like walking around this old and empty house
So hold my hand, I'll walk with you, my dear
The stairs creak as you sleep, it's keeping me awake
It's the house telling you to close your eyes

And some days I can't even dress myself
It's killing me to see you this way

'Cause though the truth may vary
This ship will carry our bodies safe to shore

Hey! Hey! Hey!

There's an old voice in my head that's holding me back
Well tell her that I miss our little talks
Soon it will be over and buried with our past
We used to play outside when we were young
And full of life and full of love.

[Video version:] Some days I don't know if I am wrong or right
[Live version:] Some days I feel like I'm wrong when I'm right
Your mind is playing tricks on you, my dear

'Cause though the truth may vary
This ship will carry our bodies safe to shore

Hey!
Don't listen to a word I say
Hey!
The screams all sound the same
Hey!

Though the truth may vary
This ship will carry our bodies safe to shore

Hey!
Hey!

You're gone, gone, gone away
I watched you disappear
All that's left is the ghost of you.
Now we're torn, torn, torn apart,
There's nothing we can do
Just let me go we'll meet again soon
Now wait, wait, wait for me
Please hang around
I'll see you when I fall asleep

Hey!
Don't listen to a word I say
Hey!
The screams all sound the same
Hey!
Though the truth may vary
This ship will carry our bodies safe to shore

Don't listen to a word I say
Hey!
The screams all sound the same
Hey!

Though the truth may vary
This ship will carry our bodies safe to shore

Though the truth may vary
This ship will carry our bodies safe to shore

Though the truth may vary
This ship will carry our bodies safe to shore
```
