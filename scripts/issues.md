* Check that adding multiple songs still work ✓
    * Using `enqueue` ✓
    * Added debug flag ✓
    * Fixed a visual mode bug ✓
* Implement seekStart ✓
    * Bound to `←` ✓
    * Might as well bind `→` to seekNext ✓
* UI
    * Show what's playing ✓
    * Show loop indicator ✓
    * Convert seconds to MM:SS for display ✓
    * Add start time 0 to main stream ✓
    * Legend for keybindings ✓
    * Deprecate title for the one the user supplied ✓
* Features ✓
    * Request current time ✓
* Parameterise into a plugin
    * Documentation
    * Screenshot/cast?
