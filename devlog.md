# devlog

Nice music to listen to:

    ls ~/Music/aimee_mann
    vlc -I rc ~/Music/aimee_mann/"01. Humpty Dumpty.mp3" --loop
    vlc -I rc ~/Music/aimee_mann/"06. Deathly.mp3" --loop
    vlc -I rc ~/Music/aimee_mann/"06. Pavlov's Bell.mp3" --loop
    vlc -I rc ~/Music/aimee_mann/"03. Red Vines.mp3" --loop
    vlc -I rc ~/Music/aimee_mann/01.\ Freeway.mp3
    
    
## 29/11/18



## 18/05/17

Seeing if you can do a loop on vlc via their rc interface...

    vlc -I rc ~/Music/aimee_mann/01.\ Freeway.mp3

    vlc -I rc ~/Music/aimee_mann/01.\ Freeway.mp3 --start-time 30 --stop-time 40 --loop

Sweet, this works!!!

    scripts/tmux_vlc_drill.sh

Okay looks like I need to use `new`:

    https://github.com/sjl/gundo.vim/blob/master/autoload/gundo.vim#L199

## 20/05/17

Alright, let's make some good headway on this.

    vlc -I rc ~/Music/aimee_mann/01.\ Freeway.mp3 --loop

Other ones:

    ls ~/Music/aimee_mann

    stop


* **13:52**: Right, let's get a telnet interface up...

vlc -I telnet --telnet-password test

nc localhost 4212

Cool, this works!

    echo -e "test\nhelp" | nc localhost 4212

    echo -e "test\nshutdown" | nc localhost 4212

Right, so flow will be:

    vlc -I telnet --telnet-password test &

    echo -e "test\nshutdown" | nc localhost 4212

Bit hacky, but will probably work...! Can use this to test/set the command...

    lsof -i:4212

Alright. Let's figure out how to play stuff without stopping and starting the server...

    vlc -I rc

    help

Cool, this works!

    playlist
    +----[ Playlist - Undefined ]
    | 2 - Playlist
    |   5 - Freeway (00:03:50)
    | 3 - Media Library
    |   4 - Freeway (00:03:50)
    +----[ End of playlist ]

    enqueue /Users/xiaodili/Music/aimee_mann/01. Freeway.mp3

    +----[ Playlist - Undefined ]
    | 2 - Playlist
    |   5 - Freeway (00:03:50)
    | 3 - Media Library
    |   4 - Freeway (00:03:50)
    +----[ End of playlist ]

Cool, looks like we can index it from there if we enqueue them just the once.

    enqueue /Users/xiaodili/Music/aimee_mann/06. Deathly.mp3

    playlist
    +----[ Playlist - Undefined ]
    | 2 - Playlist
    |   5 - Freeway (00:03:50) [played 2 times]
    |   6 - Deathly (00:05:37)
    | 3 - Media Library
    |   4 - Freeway (00:03:50)
    +----[ End of playlist ]

Would I need to get the bash fullpath?

    # http://stackoverflow.com/questions/284662/how-do-you-normalize-a-file-path-in-bash
    stat -f %N ~/Music/aimee_mann/'06. Deathly.mp3'

    vlc -I telnet --telnet-password test &

    echo -e "test\nshutdown" | nc localhost 4212

    lsof -i:4212

## 21/05/17

    vlc -I telnet --telnet-password test >$HOME/.vlc_drill.log 2>&1 &

## 21/05/17

Getting somewhere, but how would you do looping...?

    vlc -I rc

    help

    enqueue /Users/xiaodili/Music/aimee_mann/06. Deathly.mp3 --start-time 30 --stop-time 40

    enqueue /Users/xiaodili/Music/aimee_mann/06. Deathly.mp3

    playlist

    play

Doesn't look like you can... hmm. Try passing it in from vlc when starting the server?

    vlc -I telnet --telnet-password test ~/Music/aimee_mann/01.\ Freeway.mp3 --start-time 30 --stop-time 40 --loop

Cool, this works... I can just do this then, hopefully!

* **16:17**: Alright, doing a bit of jamming to red vines...

I wonder if you can do it over youtube...

    vlc -I rc

Damn, that's so cool!

    vlc -I rc https://www.youtube.com/watch?v=-bD0gtvfqoM --loop --rate 0.8

    seek 0

    voldown
    # Good volume
    ( audio volume: 102 )

    pause

    play

    rate 1

    seek 0

    vlc -I rc https://www.youtube.com/watch?v=-bD0gtvfqoM --loop --rate 1

    seek 25

    volume

    voldown

    volup

Get the intro:

    vlc -I rc https://www.youtube.com/watch?v=-bD0gtvfqoM --start-time 0 --stop-time 10 --loop --rate 0.8

    vlc -I rc https://www.youtube.com/watch?v=-bD0gtvfqoM --start-time 0 --stop-time 10 --loop --rate 0.8

    vlc -I rc https://www.youtube.com/watch?v=-bD0gtvfqoM --start-time 10 --loop --rate 0.8

    seek 11

    vlc -I rc https://www.youtube.com/watch?v=-bD0gtvfqoM --start-time 11 --loop --rate 0.7

    seek 70

    vlc -I rc https://www.youtube.com/watch?v=-bD0gtvfqoM --start-time 11 --stop-time 70 --loop --rate 0.7

    seek 11

    stop

    echo -e "test\nhelp" | nc localhost 4212

Let's get some more bindings up then...

    nc localhost 4212

    help

    get_title

    get_length

    info
    +----[ Stream 0 ]
    |
    | Bitrate: 320 kb/s
    | Type: Audio
    | Channels: Stereo
    | Sample rate: 44100 Hz
    | Codec: MPEG Audio layer 1/2 (mpga)
    |
    +----[ end of stream info ]

    stats
    +----[ begin of statistical info
    +-[Incoming]
    | input bytes read :     8838 KiB
    | input bitrate    :      315 kb/s
    | demux bytes read :     8837 KiB
    | demux bitrate    :      321 kb/s
    | demux corrupted  :        0
    | discontinuities  :        0
    |
    +-[Video Decoding]
    | video decoded    :        0
    | frames displayed :        0
    | frames lost      :        0
    |
    +-[Audio Decoding]
    | audio decoded    :     8659
    | buffers played   :     8659
    | buffers lost     :        0
    |
    +-[Streaming]
    | packets sent     :        0
    | bytes sent       :        0 KiB
    | sending bitrate  :        0 kb/s
    +----[ end of statistical info ]

    help

    status
    ( new input: file:///Users/xiaodili/Music/aimee_mann/06. Deathly.mp3 )
    ( audio volume: 90 )
    ( state playing )

    loop

Alright, this might be tricky I guess. Doing some hacky regex now...

    let status_raw = 'byeVLC media player 2.2.5.1 UmbrellaPassword: ÿûÿüWelcome, Master( new input: file:///Users/xiaodili/Music/aimee_mann/01. Humpty Dumpty.mp3 )( audio volume: 90 )( state playing )> Bye-bye!'
    very magic: \v
    let statuses = matchstr(status_raw, '\v\d\.\d')
    echo statuses
    "2.2

    let statuses = matchstr(status_raw, '\v\( state ([a-z]+) \)')
    echo statuses
    " ( state playing )

    let playing_status = matchstr(status_raw, '\v\( state \zs[a-z]+\ze \)')
    echo playing_status
    " playing

    let audio_status = matchstr(status_raw, '\v\( audio volume: \zs[0-9]+\ze \)')
    echo audio_status
    " 90

Cool. Good old `\zs` and `\ze`

## 22/05/17

    echo -e "test\nstatus" | nc localhost 4212

    echo -e "test\nget_title" | nc localhost 4212
    
## 23/05/17

    /Applications/VLC.app/Contents/MacOS/VLC -I rc

    vlc -I rc ~/Music/aimee_mann/"01. Humpty Dumpty.mp3" --loop

    ls /Users/eddieli/Music/"Aimee Mann/Live/2004 - Live at St. Ann's Warehouse/01. The Moth.mp3"

    ls /Users/eddieli/Music/"Aimee Mann/Live/2004 - Live at St. Ann's Warehouse/13. Deathly.mp3"

    vlc -I rc

    /Applications/VLC.app/Contents/MacOS/VLC -I rc

    enqueue /Users/eddieli/Music/Aimee Mann/Live/2004 - Live at St. Ann's Warehouse/13. Deathly.mp3 :start-time=40 :stop-time=45

    play

    loop

Sweet, `clear` clears the playlist!

    clear clears the playlist

    vlc -I rc https://www.youtube.com/watch?v=-bD0gtvfqoM --start-time 0 --stop-time 10 --loop --rate 0.8

Verse:

    vlc -I rc https://www.youtube.com/watch?v=-bD0gtvfqoM --start-time 10 --loop --rate 0.8

    vlc -I rc https://www.youtube.com/watch?v=-bD0gtvfqoM --start-time 10 --loop --rate 0.8

2nd Verse:

    vlc -I rc https://www.youtube.com/watch?v=-bD0gtvfqoM --start-time 10 --loop --rate 0.8

    stop

Deathly:

    vlc -I rc https://www.youtube.com/watch?v=Nay5ybrUEEY --loop --rate 0.8

    vlc -I rc https://www.youtube.com/watch?v=Nay5ybrUEEY --loop --rate 0.8 --start-time 5

Right, back to programming:

    add /Users/xiaodili/Music/aimee_mann/01. Freeway.mp3

    echo -e "test\nadd /Users/xiaodili/Music_aimee_mann/01. Freeway.mp3" | nc localhost 4212

    nc localhost 4212


add /Users/xiaodili/Music/aimee_mann/01. Freeway.mp3
clear

Damn, that's way it's not working; it doesn't know what to do with the filepath while connected through telnet!


add /Users/xiaodili/Music/aimee_mann/01. Freeway.mp3 - works

add ~/Music/aimee_mann/01. Freeway.mp3 - doesn't work. 

Okay, so needs to be absolute path then.

Also, if the mappings don't load, check that `__VlcDrill__` hasn't been saved to disk at some point. BufNewFile won't trigger if it has ([Source](https://github.com/scrooloose/nerdtree/issues/617))

add /Users/xiaodili/Music/aimee_mann/01. Freeway.mp3

## 25/05/17

    vlc -I rc ~/Music/aimee_mann/01.\ Freeway.mp3

## 28/05/17

Alright, let's finish this one off. First of all, see if VLC got updated

    brew upgrade vlc

Oh wait, I didn't use brew to download it. MacOS is still 2.2.5.1, so I guess I'll wait a bit.

    vlc -I rc

    add /Users/xiaodili/Music/aimee_mann/01. Humpty Dumpty.mp3

    clear

Cool.

    vlc -I rc

    add /Users/xiaodili/Music/aimee_mann/01. Humpty Dumpty.mp3 :start-time=5 :stop-time=10

Testing mulitple tracks. Loop doesn't work; what about repeat...?

    add /Users/xiaodili/Music/aimee_mann/01. Humpty Dumpty.mp3 :start-time=220 /Users/xiaodil/Music/aimee_mann/06. Deathly.mp3 :stop-time=10

    repeat on

    repeat off

Nope. Needs to be two separate commands?

    add /Users/xiaodili/Music/aimee_mann/01. Humpty Dumpty.mp3 :start-time=220
    enqueue /Users/xiaodili/Music/aimee_mann/06. Deathly.mp3 :stop-time=10
    loop on

Cool, this works. Second one needs to be `enqueue`.

Let's make it so you can request the current time as well.

    vlc -I rc

    add /Users/xiaodili/Music/aimee_mann/01. Humpty Dumpty.mp3 :start-time=40

    get_time
    43

Cool, this works! Aright.

Alright, let's get this one

## 29/05/17

Looks good! Right, I think that's really everything I wanted to do for this. Now find a name, then stick it in a Git Repository.

Hmm, small bug. Rate isn't being persisted on loop.


    vlc -I rc

    clear

    add /Users/xiaodili/Music/aimee_mann/01. Freeway.mp3 :start-time=210
    rate 0.7
    repeat on
    loop on

## 30/05/17

Turns out you can load it up with the rate:

    add /Users/xiaodili/Music/aimee_mann/01. Freeway.mp3 :start-time=210 :rate=0.4

## 04/06/17

Adding lilypond bin to `bashrc`...

    alias lilypond="/Applications/Lilypond.app/Contents/MacOS/Lilypond"

## 06/06/17

    vlc -I rc
    add https://www.youtube.com/watch?v=sTm7aZE6u6w :start-time=210
    enqueue https://www.youtube.com/watch?v=sTm7aZE6u6w
    status
    status

Cool, well making it play again seems to be fix the status

Using the animated paredit guide for keycapping:

* http://danmidwood.com/content/2014/11/21/animated-paredit.html
* https://github.com/keycastr/keycastr `brew cask install keycastr`

Cool! Looks neat. And it seems like it'll work.

# Inspiration

* http://stackoverflow.com/questions/4642822/commands-executed-from-vim-are-not-recognizing-bash-command-aliases
* http://vim.wikia.com/wiki/Vim_vlc_controller_and_lyric_synchronizer
* http://learnvimscriptthehardway.stevelosh.com/chapters/52.html
* https://github.com/sjl/gundo.vim/blob/master/autoload/gundo.vim
* https://wiki.videolan.org/Console
* https://github.com/terryma/vim-expand-region/blob/master/autoload/expand_region.vim
* Stupid Thing - Piano Cover: https://www.youtube.com/watch?v=Fql89drwPUI
* Flat Seventh: https://www.youtube.com/watch?v=1dIYXiZofjE
