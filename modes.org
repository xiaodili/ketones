#+STARTUP: showall

* Resources

- Popular Songs that Use Modes https://www.youtube.com/watch?v=EPIkluAb-8c
- 

* Modes

** Ionian

Eric Clapton

** Dorian

The Doors

** Phrygian

Metallica

** Lydian

Rush

** Mixolydian

Blind Melon

** Aeolian

Santana

** Locrian
